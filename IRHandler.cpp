#include "IRHandler.h"

void IRHandler::init(int detectorPin,
                     int indicatorPin,
                     double threshold,
                     ServoController &servoController,
                     bool verbose)
{
  this->detectorPin = detectorPin;
  this->indicatorPin = indicatorPin;
  this->threshold = threshold;
  this->servoController = servoController;
  this->verbose = verbose;

  prevState = UNKNOWN;
  currState = UNKNOWN;
  cacheValid = false;
  cachePosition = 0;

  pinMode(indicatorPin, OUTPUT);
}

void IRHandler::dispenseDrop() {
  servoController.write(ServoConstants::WIND);
  servoController.start();
  delay(ServoConstants::ROTATION_TIME);
  servoController.stop();
  if (verbose && !IRConstants::IR_DEBUG_MODE && !ProximityConstants::PROXIMITY_DEBUG_MODE) {
    Serial.println("INFO: Eye drop dispensed!");
  }
}

bool IRHandler::process(bool doDrop) {
  calculateState();
  digitalWrite(indicatorPin, currState == EYE_OPEN ? HIGH : LOW);

  if (doDrop && prevState == EYE_CLOSED && currState == EYE_OPEN) {
    dispenseDrop();
    return true;
  }

  return false;
}

bool IRHandler::process(double val, bool doDrop) {
  double irReading = getIRReading();
  double percentDifference = (irReading-val)/1023.0;

  if (doDrop && percentDifference > threshold) {
    dispenseDrop();
    return true;
  }

  return false;
}

double IRHandler::getIRReading() {
  double reading = analogRead(detectorPin);
  if (verbose) {
    if (IRConstants::IR_DEBUG_MODE) {
      Serial.print("i ");
      Serial.println(reading);
    }
    else if (!ProximityConstants::PROXIMITY_DEBUG_MODE) {
      Serial.print("INFO: IR Reading: ");
      Serial.println(reading);
    }
  }
  return reading;
}

void IRHandler::updateMeasurementCache(double val) {
  if (!cacheValid) {
    measurementCache[cachePosition] = val;
    if (cachePosition >= IRConstants::IR_CACHE_SIZE-1) {
      cacheValid = true;
    }
    else {
      cachePosition++;
    }
  }
  else {
    for (int i = 0; i < IRConstants::IR_CACHE_SIZE-1; i++) {
      measurementCache[i] = measurementCache[i+1];
    }
    measurementCache[IRConstants::IR_CACHE_SIZE-1] = val;
  }
}

double IRHandler::getRollingAverage() {
  double val = 0;
  for (int i = 0; i < IRConstants::IR_CACHE_SIZE; i++) {
    val += measurementCache[i];
  }
  return val/IRConstants::IR_CACHE_SIZE;
}

void IRHandler::calculateState() {
  double irReading = getIRReading();

  if (cacheValid) {
    prevState = currState;

    double rollingAverage = getRollingAverage();
    double percentDifference = (irReading-rollingAverage)/1023.0;

    if (percentDifference > threshold) {
      currState = EYE_OPEN;
    }
    else if (percentDifference < (-1*threshold)) {
      currState = EYE_CLOSED;
    }

    if (verbose && !IRConstants::IR_DEBUG_MODE && !ProximityConstants::PROXIMITY_DEBUG_MODE) {
      Serial.print("INFO: Eye State: ");
      switch(currState) {
        case 0:
          Serial.println("Unknown");
          break;
        case 1:
          Serial.println("Eye Open");
          break;
        case 2:
          Serial.println("Eye Closed");
          break;
        default:
          Serial.println("Error");
      }
    }
  }
  
  updateMeasurementCache(irReading);
}
