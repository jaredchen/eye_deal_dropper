#ifndef PROXIMITY_SENSOR_HANDLER
#define PROXIMITY_SENSOR_HANDLER

#include <Wire.h>
#include "Arduino.h"
#include "SparkFun_VCNL4040_Arduino_Library.h"
#include "EyeDealDropperConstants.hpp"

/**
 * This class is responsible for reading and processing
 * the data collected from the VCNL4040 proximity sensor
 * on the device.
 **/
class ProximitySensorHandler {
public:
  /** Call after instantiating object **/
  void init(unsigned int threshold, bool verbose = false);

  /** Client method to read from sensor and determine if object is present **/
  bool detectObject();

  /** Client method to read from proximity sensor **/
  unsigned int getProximityValue();

private:
  bool verbose;               /** Controls whether we send information over serial **/
  unsigned int threshold;     /** Sensor threshold value for object to be detected **/       
  VCNL4040 proximitySensor;   /** VCNL4040 object from VCNL4040 library;
                                  used to interface with proximity sensor
                               **/
};

#endif
