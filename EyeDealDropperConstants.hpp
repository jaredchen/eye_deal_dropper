#ifndef EYEDEALDROPPER_CONSTANTS_H
#define EYEDEALDROPPER_CONSTANTS_H

namespace IRConstants {
  const int IR_DETECTOR_PIN = 0;
  const int IR_INDICATOR_PIN = 13;
  const double IR_THRESHOLD = 0.05;
  const int IR_CACHE_SIZE = 5;
  const int IR_TIMEOUT = 10000;
  const bool IR_DEBUG_MODE = true;    /** Change to true when using LabVIEW to read/visualize IR signal from serial communication **/
  const int DROP_METHOD = 0;          /** 0 or 1 **/
}

namespace ProximityConstants {
  const double PROXIMITY_THRESHOLD = 15000;
  const bool PROXIMITY_DEBUG_MODE = true;    /** Change to true when using LabVIEW to read/visualize proximity signal from serial communication **/
}

namespace ServoConstants {
  const int SERVO_PIN = 11;
  const int WIND = 60;
  const int UNWIND = 120;
  const int ROTATION_TIME = 1000;
}

namespace ButtonConstants {
  const int WIND_BUTTON = 5;
  const int UNWIND_BUTTON = 6;
  const int START_BUTTON = 7;
}

#endif
