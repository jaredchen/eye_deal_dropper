#ifndef IR_HANDLER
#define IR_HANDLER

#include "Arduino.h"
#include "ServoController.h"
#include "EyeDealDropperConstants.hpp"

/**
 * This class is responsible for reading and processing
 * the data collected from the IR detectors on the
 * device.
 **/
class IRHandler {
public:
  /** Enum representing the state of the eye we determine using the IR sensor **/
  enum EyeState {UNKNOWN=0, EYE_OPEN=1, EYE_CLOSED=2};

  /** Call after instantiating object **/
  void init(int detectorPin,
            int indicatorPin,
            double threshold,
            ServoController &servoController,
            bool verbose = false);

  /** Client method to read from IR sensor, determine state, and drop medication.
   *  Uses internal state calculations (calculateState) to determine when to dispense drop.
   *  Returns true if drop dispensed, else false.
   **/
  bool process(bool doDrop = false);

  /** Client method to read from IR sensor, determine state, and drop medication.
   *  Compares IR reading to the value stored in parameter val and dispenses drop
   *  when difference passes the threshold.
   *  Returns true if drop dispensed, else false.
   **/
  bool process(double val, bool doDrop = false);

  /** Client method to read from IR sensor **/
  double getIRReading();

private:
  bool verbose;                   /** Controls whether we send information over serial **/
  int detectorPin;                /** Pin the IR detector is connected to **/
  int indicatorPin;               /** Pin the IR state LED is connected to **/
  double threshold;               /** Amount of change (fraction, e.g. 0.1)
                                      in signal that indicates a state change
                                      (i.e. eye blink)
                                   **/
  double measurementCache[IRConstants::IR_CACHE_SIZE];   /** Used for calculateState;
                                                             contains past sensor data
                                                          **/
  bool cacheValid;                /** Used for calculateState;
                                      indicates whether cache is filled
                                   **/
  int cachePosition;              /** Used for calculateState;
                                      indicates how far cache is filled
                                   **/
  EyeState prevState;             /** Used for calculateState;
                                      holds previous state
                                   **/
  EyeState currState;             /** Used for calculateState;
                                      holds current state
                                   **/
  ServoController servoController;      /** Used for process method;
                                            used to turn motor to dispense drop
                                         **/

  /** Helper method for calculateState to record sensor data **/
  void updateMeasurementCache(double val);

  /** Helper method for calculateState to get rolling average of past sensor data **/
  double getRollingAverage();

  /** Internal method to calculate eye state based on IR sensor data **/
  void calculateState();

  /** Helper method for process method to dispense drop using ServoController **/
  void dispenseDrop();
};

#endif
