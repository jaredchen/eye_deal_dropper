#ifndef SERVO_CONTROLLER
#define SERVO_CONTROLLER

#include "Arduino.h"
#include <Servo.h>
#include "EyeDealDropperConstants.hpp"

/**
 * This class is responsible for controlling
 * the servo motor attached to the device.
 **/
class ServoController {
public:
  /** Call after instantiating object **/
  void init(int pin, bool verbose = false);

  /** Client method to set the motor speed **/
  void write(int speed); /** 0 is full-speed
                             in one direction,
                             180 is full speed
                             in the other,
                             90 is no movement
                          **/

  /** Client method to stop the motor **/
  void stop();

  /** Client method to start the motor **/
  void start();

private:
  bool verbose;   /** Controls whether we send information over serial **/
  int pin;        /** Pin the servo motor is connected to **/
  Servo servo;    /** Servo object used to interface with the servo motor **/
};

#endif
