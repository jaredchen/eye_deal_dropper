#include "ProximitySensorHandler.h"

void ProximitySensorHandler::init(unsigned int threshold, bool verbose) {
  this->verbose = verbose;
  this->threshold = threshold;

  Wire.begin();
  if (!(proximitySensor.begin()) && verbose && !IRConstants::IR_DEBUG_MODE && !ProximityConstants::PROXIMITY_DEBUG_MODE) {
    Serial.println("ERROR: Proximity sensor not found!");
  }

  proximitySensor.powerOffProximity();
}

bool ProximitySensorHandler::detectObject() {
  bool objectDetected = getProximityValue() > threshold;

  if (verbose && !IRConstants::IR_DEBUG_MODE && !ProximityConstants::PROXIMITY_DEBUG_MODE) {
    if (objectDetected) {
      Serial.println("INFO: Proximity Sensor: Object detected");
    }
    else {
      Serial.println("INFO: Proximity Sensor: Object not detected");
    }
  }

  return objectDetected;
}

unsigned int ProximitySensorHandler::getProximityValue() {
  /** To avoid signal pollution with IR detector,
   *  we turn on the proximity sensor's IRED only
   *  when we take a measurement. **/
  proximitySensor.powerOnProximity();
  delay(10);  // Wait 10 ms for IRED to be ready

  unsigned int proximityValue = proximitySensor.getProximity();

  if (verbose) {
    if (ProximityConstants::PROXIMITY_DEBUG_MODE) {
      Serial.print("p ");
      Serial.println(proximityValue);
    }
    else if (!IRConstants::IR_DEBUG_MODE) {
      Serial.print("INFO: Proximity Value: ");
      Serial.println(proximityValue);
    }
  }

  proximitySensor.powerOffProximity();

  return proximityValue;
}