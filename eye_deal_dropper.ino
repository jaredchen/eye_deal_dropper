#include "IRHandler.h"
#include "ProximitySensorHandler.h"
#include "ServoController.h"
#include "EyeDealDropperConstants.hpp"

#define VERBOSE 1

IRHandler irHandler;
ProximitySensorHandler proximitySensorHandler;
ServoController servoController;

void setup()
{
  if (VERBOSE) {
    Serial.begin(9600);
  }

  servoController.init(ServoConstants::SERVO_PIN,
                       VERBOSE);
  irHandler.init(IRConstants::IR_DETECTOR_PIN,
                 IRConstants::IR_INDICATOR_PIN,
                 IRConstants::IR_THRESHOLD,
                 servoController,
                 VERBOSE);
  proximitySensorHandler.init(ProximityConstants::PROXIMITY_THRESHOLD,
                              VERBOSE);
  
  pinMode(ButtonConstants::WIND_BUTTON, INPUT);
  pinMode(ButtonConstants::UNWIND_BUTTON, INPUT);
  pinMode(ButtonConstants::START_BUTTON, INPUT);

  if (VERBOSE && !IRConstants::IR_DEBUG_MODE && !ProximityConstants::PROXIMITY_DEBUG_MODE) {
    Serial.println("Finished initializing.");
    Serial.println();
  }
}

void loop()
{
  if (IRConstants::IR_DEBUG_MODE) {
    irHandler.process();
  }

  bool isCapClosed = proximitySensorHandler.detectObject();
  
  if (!isCapClosed) {
    if (digitalRead(ButtonConstants::WIND_BUTTON) == HIGH) {
      servoController.write(ServoConstants::WIND);
      servoController.start();
    }
    else if (digitalRead(ButtonConstants::UNWIND_BUTTON) == HIGH) {
      servoController.write(ServoConstants::UNWIND);
      servoController.start();
    }
    else {
      servoController.stop();
      if (digitalRead(ButtonConstants::START_BUTTON) == HIGH) {
        unsigned long startTime = millis();
        switch (IRConstants::DROP_METHOD) {
          case 0:
          {
            double eyeClosedReading = irHandler.getIRReading();
            /* Wait for eye to open. If IR_TIMEOUT ms passes, break. */
            while (millis() - startTime < IRConstants::IR_TIMEOUT) {
              if (irHandler.process(eyeClosedReading, true)) break;
            }
            break;
          }
          case 1:
          default:
            /* Wait for eye to open. If IR_TIMEOUT ms passes, break. */
            while (millis() - startTime < IRConstants::IR_TIMEOUT) {
              if (irHandler.process(true)) break;
            }
        }
      }
    }
  }

  delay(1);
}