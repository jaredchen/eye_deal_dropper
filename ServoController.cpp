#include "ServoController.h"

void ServoController::init(int pin, bool verbose) {
  this->pin = pin;
  this->verbose = verbose;
}

void ServoController::write(int speed) {
  servo.write(speed);
}

void ServoController::stop() {
  servo.detach();
}

void ServoController::start() {
  if (!servo.attached()) {
    servo.attach(pin, 800, 2200);
  }
}